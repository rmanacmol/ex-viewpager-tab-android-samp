package mummysmarket.mobileapp.viewpagertab

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val childFragments: Array<Fragment>

    init {
        childFragments = arrayOf(ChildFragment1(), ChildFragment2(), ChildFragment3())
    }

    override fun getItem(position: Int): Fragment {
        return childFragments[position]
    }

    override fun getCount(): Int {
        return childFragments.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        val title = getItem(position)
        return title.toString()

    }

}
