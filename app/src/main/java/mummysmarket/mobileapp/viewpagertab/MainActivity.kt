package mummysmarket.mobileapp.viewpagertab

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewPager.adapter = ViewPagerAdapter(supportFragmentManager)
        viewPager.setOnTouchListener({ view: View, _: MotionEvent -> true })

        next.setOnClickListener {
            viewPager.currentItem = viewPager.currentItem + 1
        }

        previous.setOnClickListener {
            viewPager.currentItem = viewPager.currentItem - 1
        }

    }
}
